package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxArea;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxAreaMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxAreaService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxAreaService实现类
*@Version
*/
@Service
public class  WxAreaServiceImpl extends BasicServerImpl<WxArea,  WxAreaMapperDao> implements  WxAreaService {

@Resource
@Override
public void setDao( WxAreaMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
