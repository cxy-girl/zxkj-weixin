package com.zxkj.ssm.weixin.shop.exception.handler;
import static com.alibaba.fastjson.JSON.toJSONString;
import com.zxkj.ssm.weixin.shop.exception.ServiceException;
import com.zxkj.ssm.weixin.shop.model.ResponseResult;
import com.zxkj.ssm.weixin.shop.exception.BusinessException;
import com.zxkj.ssm.weixin.shop.utils.ResponseUtils;
import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import java.nio.file.AccessDeniedException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
/**
 * @author 程序媛
 * @2018年6月24日
 * @Description: Springboot统一异常处理  以json形式返回到页面
 */
@Log4j
@Configuration
public class ExceptionHandlerResolvers extends WebMvcConfigurerAdapter{
    @Override
        public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
             exceptionResolvers.add((request, response,handler, ex) -> {
                 ResponseResult result=new ResponseResult();
                if(ex instanceof ServiceException){
                result.setStatusCode(HttpStatus.EXPECTATION_FAILED.value()).setMessage(ex.getMessage());
                }else if(ex instanceof NoHandlerFoundException){
                    result.setStatusCode(HttpStatus.NOT_FOUND.value()).setMessage("方法未找到!");
                }else if(ex instanceof  IllegalArgumentException || ex instanceof BusinessException){
                    result.setStatusCode(HttpStatus.BAD_REQUEST.value()).setMessage("参数错误!");
                }else if(ex instanceof SQLException){
                    result.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).setMessage(ex.getMessage());
                }else if(ex instanceof AccessDeniedException){
                    result.setStatusCode(HttpStatus.FORBIDDEN.value()).setMessage(ex.getMessage());
                }else if(ex instanceof MethodArgumentTypeMismatchException){
                    result.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).setMessage("方法参数错误!");
                }else{
                    result.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).setMessage(ex.getMessage());
                    String message;
                    if(handler instanceof HandlerMethod){
                        HandlerMethod method=(HandlerMethod)handler;
                        message=String.format("接口 [%s] 出现异常、方法:%s.%s,异常信息:%s",request.getRequestURI(),
                                method.getBean().getClass().getName(),
                                method.getMethod().getName(),
                                ex.getMessage());
                    }else{
                        message= ex.getMessage();
                    }
                    log.error(message);
                }
                 result.setTime(new Date());
                 ResponseUtils.renderJson(response, toJSONString(result));
                 return new ModelAndView();
             });
        }
}