package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxAddress;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxAddressMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxAddressService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxAddressService实现类
*@Version
*/
@Service
public class  WxAddressServiceImpl extends BasicServerImpl<WxAddress,  WxAddressMapperDao> implements  WxAddressService {

@Resource
@Override
public void setDao( WxAddressMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
