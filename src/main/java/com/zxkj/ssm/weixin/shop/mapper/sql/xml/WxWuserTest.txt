目标package: com.zxkj.ssm.weixin.shop.mapper.sql.xml

当前时间：
2018-7-10
17:23:22
2018-07-10 17:23:22

所有配置的属性信息:
targetPackage - com.zxkj.ssm.weixin.shop.mapper.sql.xml
templateFormatter - com.zxkj.ssm.weixin.shop.mybatis.generator.formatter.FreemarkerTemplateFormatter
templatePath - template/test/test-one.ftl
targetProject - D:/EclipseWorkspace2/zxkj-weixin/src/main/java
fileName - ${tableClass.shortClassName}Test.txt

实体和表的信息：
表名：wx_wuser
变量名：wxWuser
小写名：wxwuser
类名：WxWuser
全名：com.zxkj.ssm.weixin.shop.entity.WxWuser
包名：com.zxkj.ssm.weixin.shop.entity

列的信息：
=====================================
主键：
    -------------------------------------
    列名：wu_id
    列类型：INTEGER
    字段名：wuId
    注释：主键
    类型包名：java.lang
    类型短名：Integer
    类型全名：java.lang.Integer
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0

基础列：
    -------------------------------------
    列名：wu_nickname
    列类型：VARCHAR
    字段名：wuNickname
    注释：昵称
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：255
    列精度：0
    -------------------------------------
    列名：wu_openid
    列类型：VARCHAR
    字段名：wuOpenid
    注释：微信号
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：255
    列精度：0
    -------------------------------------
    列名：wu_sex
    列类型：CHAR
    字段名：wuSex
    注释：性别
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：5
    列精度：0
    -------------------------------------
    列名：wu_city
    列类型：VARCHAR
    字段名：wuCity
    注释：城市
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：50
    列精度：0
    -------------------------------------
    列名：wu_country
    列类型：VARCHAR
    字段名：wuCountry
    注释：国家
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：50
    列精度：0
    -------------------------------------
    列名：wu_province
    列类型：VARCHAR
    字段名：wuProvince
    注释：省份
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：50
    列精度：0
    -------------------------------------
    列名：wu_language
    列类型：VARCHAR
    字段名：wuLanguage
    注释：语言
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：50
    列精度：0
    -------------------------------------
    列名：wu_headimg_url
    列类型：VARCHAR
    字段名：wuHeadimgUrl
    注释：头像
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：255
    列精度：0
    -------------------------------------
    列名：wu_subscribe_time
    列类型：TIMESTAMP
    字段名：wuSubscribeTime
    注释：关注时间
    类型包名：java.util
    类型短名：Date
    类型全名：java.util.Date
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：19
    列精度：0
    -------------------------------------
    列名：wu_unsubscribe_time
    列类型：TIMESTAMP
    字段名：wuUnsubscribeTime
    注释：取消关注时间
    类型包名：java.util
    类型短名：Date
    类型全名：java.util.Date
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：19
    列精度：0
    -------------------------------------
    列名：wu_groupid
    列类型：INTEGER
    字段名：wuGroupid
    注释：微信分组id
    类型包名：java.lang
    类型短名：Integer
    类型全名：java.lang.Integer
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0
    -------------------------------------
    列名：wu_status
    列类型：INTEGER
    字段名：wuStatus
    注释：状态
    类型包名：java.lang
    类型短名：Integer
    类型全名：java.lang.Integer
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0

Blob列：

=====================================
全部列：
列名 - 字段名
    wu_id - wuId
    wu_nickname - wuNickname
    wu_openid - wuOpenid
    wu_sex - wuSex
    wu_city - wuCity
    wu_country - wuCountry
    wu_province - wuProvince
    wu_language - wuLanguage
    wu_headimg_url - wuHeadimgUrl
    wu_subscribe_time - wuSubscribeTime
    wu_unsubscribe_time - wuUnsubscribeTime
    wu_groupid - wuGroupid
    wu_status - wuStatus
