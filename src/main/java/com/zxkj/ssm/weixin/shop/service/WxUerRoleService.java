package com.zxkj.ssm.weixin.shop.service;
import  com.zxkj.ssm.weixin.shop.basic.BasicService;
import  com.zxkj.ssm.weixin.shop.entity.WxUerRole;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxUerRoleService
*@Version
*/
public interface WxUerRoleService extends BasicService<WxUerRole> {

    }
