package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxOrderDetails;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxOrderDetailsMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxOrderDetailsService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxOrderDetailsService实现类
*@Version
*/
@Service
public class  WxOrderDetailsServiceImpl extends BasicServerImpl<WxOrderDetails,  WxOrderDetailsMapperDao> implements  WxOrderDetailsService {

@Resource
@Override
public void setDao( WxOrderDetailsMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
