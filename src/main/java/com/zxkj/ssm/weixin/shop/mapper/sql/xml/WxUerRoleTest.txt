目标package: com.zxkj.ssm.weixin.shop.mapper.sql.xml

当前时间：
2018-7-10
17:23:22
2018-07-10 17:23:22

所有配置的属性信息:
targetPackage - com.zxkj.ssm.weixin.shop.mapper.sql.xml
templateFormatter - com.zxkj.ssm.weixin.shop.mybatis.generator.formatter.FreemarkerTemplateFormatter
templatePath - template/test/test-one.ftl
targetProject - D:/EclipseWorkspace2/zxkj-weixin/src/main/java
fileName - ${tableClass.shortClassName}Test.txt

实体和表的信息：
表名：wx_uer_role
变量名：wxUerRole
小写名：wxuerrole
类名：WxUerRole
全名：com.zxkj.ssm.weixin.shop.entity.WxUerRole
包名：com.zxkj.ssm.weixin.shop.entity

列的信息：
=====================================
主键：
    -------------------------------------
    列名：user_role_id
    列类型：INTEGER
    字段名：userRoleId
    注释：主键
    类型包名：java.lang
    类型短名：Integer
    类型全名：java.lang.Integer
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0

基础列：
    -------------------------------------
    列名：user_id
    列类型：INTEGER
    字段名：userId
    注释：用户ID
    类型包名：java.lang
    类型短名：Integer
    类型全名：java.lang.Integer
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0
    -------------------------------------
    列名：role_id
    列类型：INTEGER
    字段名：roleId
    注释：角色ID
    类型包名：java.lang
    类型短名：Integer
    类型全名：java.lang.Integer
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0

Blob列：

=====================================
全部列：
列名 - 字段名
    user_role_id - userRoleId
    user_id - userId
    role_id - roleId
