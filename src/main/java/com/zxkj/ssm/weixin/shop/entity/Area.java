package com.zxkj.ssm.weixin.shop.entity;

import lombok.Data;

import java.io.Serializable;
/**
 *   区域  省  、市 、县
 */
@Data
public class Area implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;
	private String code;
	private String name;
	private String pcode;


	public Area() {
	}

	public Area(String code, String name, String pcode) {
		this.code = code;
		this.name = name;
		this.pcode = pcode;
	}

	public void clear() {
		id = null;
		code = null;
		name = null;
		pcode = null;
	}
}
