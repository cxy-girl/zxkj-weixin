package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxMenu;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxMenuMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxMenuService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxMenuService实现类
*@Version
*/
@Service
public class  WxMenuServiceImpl extends BasicServerImpl<WxMenu,  WxMenuMapperDao> implements  WxMenuService {

@Resource
@Override
public void setDao( WxMenuMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
