package com.zxkj.ssm.weixin.shop.mapper.dao;
import java.util.List;
import java.util.Map;
import com.zxkj.ssm.weixin.shop.basic.BasicDao;
import com.zxkj.ssm.weixin.shop.dto.AreaDTO;
/**
 * 区域管理接口
 */
public interface AreaMapperDao extends BasicDao<AreaDTO>{
	public List<AreaDTO>	findAllAreaList()throws Exception;
	
	public List<AreaDTO>  pageQueryAreaList(Map<String, Object> map)throws Exception;
	
	public AreaDTO selectOneResult()throws Exception;
	
	public int findCountArea()throws Exception;
}
