package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxRoleMenu;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxRoleMenuMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxRoleMenuService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxRoleMenuService实现类
*@Version
*/
@Service
public class  WxRoleMenuServiceImpl extends BasicServerImpl<WxRoleMenu,  WxRoleMenuMapperDao> implements  WxRoleMenuService {

@Resource
@Override
public void setDao( WxRoleMenuMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
