package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxWuser;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxWuserMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxWuserService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxWuserService实现类
*@Version
*/
@Service
public class  WxWuserServiceImpl extends BasicServerImpl<WxWuser,  WxWuserMapperDao> implements  WxWuserService {

@Resource
@Override
public void setDao( WxWuserMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
