目标package: com.zxkj.ssm.weixin.shop.mapper.sql.xml

当前时间：
2018-7-10
17:23:22
2018-07-10 17:23:22

所有配置的属性信息:
targetPackage - com.zxkj.ssm.weixin.shop.mapper.sql.xml
templateFormatter - com.zxkj.ssm.weixin.shop.mybatis.generator.formatter.FreemarkerTemplateFormatter
templatePath - template/test/test-one.ftl
targetProject - D:/EclipseWorkspace2/zxkj-weixin/src/main/java
fileName - ${tableClass.shortClassName}Test.txt

实体和表的信息：
表名：wx_menu
变量名：wxMenu
小写名：wxmenu
类名：WxMenu
全名：com.zxkj.ssm.weixin.shop.entity.WxMenu
包名：com.zxkj.ssm.weixin.shop.entity

列的信息：
=====================================
主键：
    -------------------------------------
    列名：menu_id
    列类型：INTEGER
    字段名：menuId
    注释：主键
    类型包名：java.lang
    类型短名：Integer
    类型全名：java.lang.Integer
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0

基础列：
    -------------------------------------
    列名：menu_name
    列类型：VARCHAR
    字段名：menuName
    注释：菜单名称
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：50
    列精度：0
    -------------------------------------
    列名：menu_sort
    列类型：INTEGER
    字段名：menuSort
    注释：序号
    类型包名：java.lang
    类型短名：Integer
    类型全名：java.lang.Integer
    是否主键：false
    是否可空：true
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0
    -------------------------------------
    列名：menu_code
    列类型：VARCHAR
    字段名：menuCode
    注释：权限字符：主要用于shiro权限框架
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：true
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：50
    列精度：0
    -------------------------------------
    列名：menu_url
    列类型：VARCHAR
    字段名：menuUrl
    注释：菜单url
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：true
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：100
    列精度：0
    -------------------------------------
    列名：menu_icon
    列类型：VARCHAR
    字段名：menuIcon
    注释：菜单icon图标
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：true
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：100
    列精度：0
    -------------------------------------
    列名：menu_pid
    列类型：INTEGER
    字段名：menuPid
    注释：父菜单id
    类型包名：java.lang
    类型短名：Integer
    类型全名：java.lang.Integer
    是否主键：false
    是否可空：true
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0
    -------------------------------------
    列名：menu_create_time
    列类型：TIMESTAMP
    字段名：menuCreateTime
    注释：菜单创建时间
    类型包名：java.util
    类型短名：Date
    类型全名：java.util.Date
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：19
    列精度：0

Blob列：

=====================================
全部列：
列名 - 字段名
    menu_id - menuId
    menu_name - menuName
    menu_sort - menuSort
    menu_code - menuCode
    menu_url - menuUrl
    menu_icon - menuIcon
    menu_pid - menuPid
    menu_create_time - menuCreateTime
