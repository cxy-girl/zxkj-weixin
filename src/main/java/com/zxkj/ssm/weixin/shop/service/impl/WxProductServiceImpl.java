package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxProduct;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxProductMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxProductService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxProductService实现类
*@Version
*/
@Service
public class  WxProductServiceImpl extends BasicServerImpl<WxProduct,  WxProductMapperDao> implements  WxProductService {

@Resource
@Override
public void setDao( WxProductMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
