package com.zxkj.ssm.weixin.shop.mapper.dao;
import com.zxkj.ssm.weixin.shop.basic.BasicDao;
import com.zxkj.ssm.weixin.shop.entity.WxConfig;

/**
* 通用 Mapper 代码生成器
*/
public interface WxConfigMapperDao extends BasicDao<WxConfig> {

}




