package com.zxkj.ssm.weixin.shop.cache.impl;
import com.zxkj.ssm.weixin.shop.cache.provider.CacheProvider;
import org.slf4j.LoggerFactory;
import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.Properties;
import  static com.zxkj.ssm.weixin.shop.basic.Base.trimToEmpty;


/**
 * 系统管理类.
 */
public class SystemManager {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SystemManager.class);
	private static Properties p = new Properties();
    private static CacheProvider cacheProvider = new EhcacheCacheProvider();
	private static SystemManager instance;

    @PostConstruct
    public void afterPropertiesSet(){
        instance = this;
    }


    public CacheProvider getCacheProvider() {
        return cacheProvider;
    }

    public void setCacheProvider(CacheProvider cacheProvider) {
        SystemManager.cacheProvider = cacheProvider;
    }
    private static String buildKey(String key) {
        return trimToEmpty(key);
    }
    public  static void putCacheObject(String key, Serializable cacheObject){
        String key1 = buildKey(key);
        if(cacheObject == null){
            cacheProvider.remove(key1);
        } else {
            cacheProvider.put(key1, cacheObject);
        }
    }
    public static <T extends Serializable> T getCacheObject(String key){
        return (T)cacheProvider.get(buildKey(key));
    }

}
