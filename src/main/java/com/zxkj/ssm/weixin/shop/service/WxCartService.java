package com.zxkj.ssm.weixin.shop.service;
import  com.zxkj.ssm.weixin.shop.basic.BasicService;
import  com.zxkj.ssm.weixin.shop.entity.WxCart;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxCartService
*@Version
*/
public interface WxCartService extends BasicService<WxCart> {

    }
