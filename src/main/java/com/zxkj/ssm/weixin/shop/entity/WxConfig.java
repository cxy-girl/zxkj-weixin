package com.zxkj.ssm.weixin.shop.entity;

import com.zxkj.ssm.weixin.shop.model.PagerModel;
import java.io.Serializable;

public class WxConfig extends PagerModel implements Serializable {
    private Integer cfgId;

    /**
     * 配置key
     *
     * @mbg.generated
     */
    private String cfgKey;

    /**
     * 配置value
     *
     * @mbg.generated
     */
    private String cfgVaule;

    private static final long serialVersionUID = 1L;

    public Integer getCfgId() {
        return cfgId;
    }

    public void setCfgId(Integer cfgId) {
        this.cfgId = cfgId;
    }

    public String getCfgKey() {
        return cfgKey;
    }

    public void setCfgKey(String cfgKey) {
        this.cfgKey = cfgKey;
    }

    public String getCfgVaule() {
        return cfgVaule;
    }

    public void setCfgVaule(String cfgVaule) {
        this.cfgVaule = cfgVaule;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", cfgId=").append(cfgId);
        sb.append(", cfgKey=").append(cfgKey);
        sb.append(", cfgVaule=").append(cfgVaule);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        WxConfig other = (WxConfig) that;
        return (this.getCfgId() == null ? other.getCfgId() == null : this.getCfgId().equals(other.getCfgId()))
            && (this.getCfgKey() == null ? other.getCfgKey() == null : this.getCfgKey().equals(other.getCfgKey()))
            && (this.getCfgVaule() == null ? other.getCfgVaule() == null : this.getCfgVaule().equals(other.getCfgVaule()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getCfgId() == null) ? 0 : getCfgId().hashCode());
        result = prime * result + ((getCfgKey() == null) ? 0 : getCfgKey().hashCode());
        result = prime * result + ((getCfgVaule() == null) ? 0 : getCfgVaule().hashCode());
        return result;
    }
}