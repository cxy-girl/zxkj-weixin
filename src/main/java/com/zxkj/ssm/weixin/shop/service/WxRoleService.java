package com.zxkj.ssm.weixin.shop.service;
import  com.zxkj.ssm.weixin.shop.basic.BasicService;
import  com.zxkj.ssm.weixin.shop.entity.WxRole;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxRoleService
*@Version
*/
public interface WxRoleService extends BasicService<WxRole> {

    }
