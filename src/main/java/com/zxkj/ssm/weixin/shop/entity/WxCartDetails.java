package com.zxkj.ssm.weixin.shop.entity;

import com.zxkj.ssm.weixin.shop.model.PagerModel;
import java.io.Serializable;
import java.util.Date;

public class WxCartDetails extends PagerModel implements Serializable {
    /**
     * 主键
     *
     * @mbg.generated
     */
    private Integer cartDetailsId;

    /**
     * 购物车ID
     *
     * @mbg.generated
     */
    private Integer cartId;

    /**
     * 商品ID
     *
     * @mbg.generated
     */
    private Integer productId;

    /**
     * 商品数量
     *
     * @mbg.generated
     */
    private Integer productTotal;

    /**
     * 创建时间
     *
     * @mbg.generated
     */
    private Date cartDetailsCreateTime;

    private static final long serialVersionUID = 1L;

    public Integer getCartDetailsId() {
        return cartDetailsId;
    }

    public void setCartDetailsId(Integer cartDetailsId) {
        this.cartDetailsId = cartDetailsId;
    }

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getProductTotal() {
        return productTotal;
    }

    public void setProductTotal(Integer productTotal) {
        this.productTotal = productTotal;
    }

    public Date getCartDetailsCreateTime() {
        return cartDetailsCreateTime;
    }

    public void setCartDetailsCreateTime(Date cartDetailsCreateTime) {
        this.cartDetailsCreateTime = cartDetailsCreateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", cartDetailsId=").append(cartDetailsId);
        sb.append(", cartId=").append(cartId);
        sb.append(", productId=").append(productId);
        sb.append(", productTotal=").append(productTotal);
        sb.append(", cartDetailsCreateTime=").append(cartDetailsCreateTime);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        WxCartDetails other = (WxCartDetails) that;
        return (this.getCartDetailsId() == null ? other.getCartDetailsId() == null : this.getCartDetailsId().equals(other.getCartDetailsId()))
            && (this.getCartId() == null ? other.getCartId() == null : this.getCartId().equals(other.getCartId()))
            && (this.getProductId() == null ? other.getProductId() == null : this.getProductId().equals(other.getProductId()))
            && (this.getProductTotal() == null ? other.getProductTotal() == null : this.getProductTotal().equals(other.getProductTotal()))
            && (this.getCartDetailsCreateTime() == null ? other.getCartDetailsCreateTime() == null : this.getCartDetailsCreateTime().equals(other.getCartDetailsCreateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getCartDetailsId() == null) ? 0 : getCartDetailsId().hashCode());
        result = prime * result + ((getCartId() == null) ? 0 : getCartId().hashCode());
        result = prime * result + ((getProductId() == null) ? 0 : getProductId().hashCode());
        result = prime * result + ((getProductTotal() == null) ? 0 : getProductTotal().hashCode());
        result = prime * result + ((getCartDetailsCreateTime() == null) ? 0 : getCartDetailsCreateTime().hashCode());
        return result;
    }
}