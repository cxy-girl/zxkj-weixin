package com.zxkj.ssm.weixin.shop.mybatis.generator.utils;
import java.util.HashMap;
import java.util.Map;
/**
 *@Author 程序媛
 *@Date 2018/6/30 23:42
 *@Description  代码生成类
 *@Version
 */
public class Generator {

	// 根据命名规范，只修改此常量值即可
	private static String MODULE;
	private static String DATABASE;
	private static String TABLE_PREFIX;
	private static String PACKAGE_NAME ;
	private static String JDBC_DRIVER ;
	private static String JDBC_URL;
	private static String JDBC_USERNAME;
	private static String JDBC_PASSWORD;
	private static String generatorConfigTemplatePath;
	private static String serviceTemplatePath;
	private static String serviceImplTemplatePath;
	// 需要insert后返回主键的表配置，key:表名,value:主键名
	private static Map<String, String> LAST_INSERT_ID_TABLES = new HashMap<>();
	static {
		LAST_INSERT_ID_TABLES.put("upms_user", "user_id");
		MODULE=PropertiesFileUtil.getInstance("generator").get("project.model.name");
		DATABASE = PropertiesFileUtil.getInstance("generator").get("jdbc.database");
		TABLE_PREFIX = PropertiesFileUtil.getInstance("generator").get("db.table.prefix");
		PACKAGE_NAME= PropertiesFileUtil.getInstance("generator").get("package.name.prefix");
		JDBC_DRIVER = PropertiesFileUtil.getInstance("generator").get("generator.jdbc.driver");
		JDBC_URL = PropertiesFileUtil.getInstance("generator").get("generator.jdbc.url");
		JDBC_USERNAME = PropertiesFileUtil.getInstance("generator").get("generator.jdbc.username");
		JDBC_PASSWORD = PropertiesFileUtil.getInstance("generator").get("generator.jdbc.password");
		generatorConfigTemplatePath =PropertiesFileUtil.getInstance("generator").get("generator.config.template.path");
		serviceTemplatePath =PropertiesFileUtil.getInstance("generator").get("service.template.path");
		serviceImplTemplatePath =PropertiesFileUtil.getInstance("generator").get("serviceImpl.template.path");
	}

	/**
	 * 自动代码生成
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		MybatisGeneratorUtil.generator( JDBC_DRIVER,
										JDBC_URL,
										JDBC_USERNAME,
										JDBC_PASSWORD,
										MODULE,
										DATABASE,
										TABLE_PREFIX,
										PACKAGE_NAME,
										LAST_INSERT_ID_TABLES,
										serviceImplTemplatePath,
										serviceTemplatePath,
										generatorConfigTemplatePath);

	}

}
