package com.zxkj.ssm.weixin.shop.service;
import  com.zxkj.ssm.weixin.shop.basic.BasicService;
import  com.zxkj.ssm.weixin.shop.entity.WxWuser;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxWuserService
*@Version
*/
public interface WxWuserService extends BasicService<WxWuser> {

    }
