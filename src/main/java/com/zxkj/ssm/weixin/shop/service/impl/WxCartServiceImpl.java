package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxCart;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxCartMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxCartService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxCartService实现类
*@Version
*/
@Service
public class  WxCartServiceImpl extends BasicServerImpl<WxCart,  WxCartMapperDao> implements  WxCartService {

@Resource
@Override
public void setDao( WxCartMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
