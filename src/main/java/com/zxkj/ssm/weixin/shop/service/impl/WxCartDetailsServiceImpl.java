package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxCartDetails;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxCartDetailsMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxCartDetailsService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxCartDetailsService实现类
*@Version
*/
@Service
public class  WxCartDetailsServiceImpl extends BasicServerImpl<WxCartDetails,  WxCartDetailsMapperDao> implements  WxCartDetailsService {

@Resource
@Override
public void setDao( WxCartDetailsMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
